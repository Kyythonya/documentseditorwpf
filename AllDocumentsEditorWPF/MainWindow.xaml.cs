﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace AllDocumentsEditorWPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PrintButtonClick(object sender, RoutedEventArgs e)
        {
            if (cityBox.Text == string.Empty || maleCitizenAddressBox.Text == string.Empty || maleCitizenFIOBox.Text == string.Empty || femaleCitizenAddressBox.Text == string.Empty 
                || femaleCitizenFIOBox.Text == string.Empty || datePicker.Text == string.Empty)
            {
                MessageBox.Show("Заполните все поля!");
                return;
            }

            PrintDialog printDialog = new PrintDialog();
            IDocumentPaginatorSource documentPaginatorSource = marriageContractDocument;

            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintDocument(documentPaginatorSource.DocumentPaginator, "Marriage Contract");
            }
          
        }
    }
}
